package com.root.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(indexName = "cart")
public class Cart {

	@Id
	@Field(type = FieldType.Keyword)
	@JsonIgnore
	private String id;
	
	@JsonIgnore
	private String userId;
	
	private List<Product> product;
	private int totalproducts;
	private double totalamount;

	public Cart() { }

	
	public Cart(String id, String userId, List<Product> product, int totalproducts, double totalamount) {
		super();
		this.id = id;
		this.userId = userId;
		this.product = product;
		this.totalproducts = totalproducts;
		this.totalamount = totalamount;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	public int getTotalproducts() {
		return totalproducts;
	}

	public void setTotalproducts(int totalproducts) {
		this.totalproducts = totalproducts;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}


	@Override
	public String toString() {
		return "Cart [id=" + id + ", userId=" + userId + ", product=" + product + ", totalproducts=" + totalproducts
				+ ", totalamount=" + totalamount + "]";
	}

	
}
