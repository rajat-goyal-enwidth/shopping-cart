package com.root.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


//@Entity
//@Table(name = "user")
@Document(indexName = "users")
public class DAOUser {

	@Id
	@Field(type = FieldType.Keyword)
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	@Field(type = FieldType.Text)
	private String username;
	
	@Field(type = FieldType.Text)
	private String email;
	
	@Field(type = FieldType.Text)
	private String password;
	
	@Field(type = FieldType.Text)
	private String role;
	
//	@Field(type = FieldType.Text)
	private String cartId;
//	
	public DAOUser() {
		// TODO Auto-generated constructor stub
	}
	
	public DAOUser(String id, String username, String email, String password, String role, String cartId) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.role = role;
		this.cartId = cartId;
	}


	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	
}
