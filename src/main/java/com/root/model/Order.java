package com.root.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(indexName = "orders")
public class Order {

	@Id
	@Field(type = FieldType.Keyword)
	@JsonIgnore
	private String orderId;
	
	@JsonIgnore
	private String userId;
	
	private List<Product> products;
	private double totalamount;
	private int totalproducts;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}
	public int getTotalproducts() {
		return totalproducts;
	}
	public void setTotalproducts(int totalproducts) {
		this.totalproducts = totalproducts;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
}
