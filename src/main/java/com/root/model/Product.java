package com.root.model;

import org.springframework.data.annotation.Id; 
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(indexName = "products")
public class Product {

	@Id
	@Field(type = FieldType.Keyword)
	@JsonIgnore
	private String id;
	private String productName;
	private double productPrice;
	private String productDetails;
	private int quantity;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}
	
	public Product(String id, String productName, double productPrice, String productDetails, int quantity) {
		super();
		this.id = id;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productDetails = productDetails;
		this.quantity = quantity;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", productDetails=" + productDetails + ", quantity=" + quantity + "]";
	}
	
	
}
