package com.root.exception;

public class ProductExceptions extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ProductExceptions(String str)
	{
		super(str);
	}
	
}
