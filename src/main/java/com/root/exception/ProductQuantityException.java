package com.root.exception;

public class ProductQuantityException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ProductQuantityException(String str)
	{
		super(str);
	}
}
