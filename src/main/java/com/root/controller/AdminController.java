package com.root.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.root.exception.ProductExceptions;
import com.root.model.DAOUser;
import com.root.model.Product;
import com.root.repository.OrderRepository;
import com.root.repository.UserRepository;
import com.root.service.AdminService;
import com.root.service.ProductService;

@RestController
@RequestMapping("/shoppingcart")
public class AdminController {

	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	UserRepository userRepo;
	@GetMapping("/admin")
	public String getAdmin() {
		return "Hello Admin";
	}

	
	@GetMapping("/admin/finduser/{username}")
	public DAOUser findUser(@PathVariable String username) {
		return adminService.findUser(username);
	}

	
	@DeleteMapping("/admin/deleteuser/{username}")
	public String deleteUser(@PathVariable String username)
	{
		String message = adminService.deleteUser(username);
		return message;
	}
	
	
	@PostMapping("/admin/addproduct")
	public String saveProduct(@RequestBody Product product)
	{
		String message = productService.saveProduct(product);
		return message;
	}
	
	
	@PutMapping("/admin/updateproduct/{id}")
	public Product updateProduct(@PathVariable String id, @RequestParam("productPrice") double productprice) throws ProductExceptions
	{
		return productService.updateProductPrice(id, productprice);
	}
	
	
	@DeleteMapping("/admin/deleteproduct/{id}")
	public String deleteProduct(@PathVariable String id) throws ProductExceptions
	{
		return productService.deleteProduct(id);
	}
	
	
	@GetMapping("/admin/findproductbyid/{id}")
	public ResponseEntity<Product> findProduct(@PathVariable String id)
	{
		Product product = productService.findproductbyid(id);
		return new ResponseEntity<Product>(product,HttpStatus.OK);
	}
	
	
	@GetMapping("/admin/findallproducts")
	public List<Product> findallProduct()
	{
		return productService.findallproducts();
	}
	
	
	@GetMapping("/admin/findproductbyname/{productName}")
	public Product findProductByname(@PathVariable String productName) throws ProductExceptions
	{
		return productService.findproductByname(productName);
		
	}
	
	@PostMapping("/admin/updatequantity/{id}")
	public String update(@PathVariable String id, @RequestParam int quantity)
	{
		return productService.updateProductQuantity(id,quantity);
	}
	
	
	@DeleteMapping("/admin/deleteorder/{id}")
	public void remove(@PathVariable String id)
	{
		orderRepo.deleteById(id);
	}
	
}
