package com.root.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.root.exception.ProductExceptions;
import com.root.model.Cart;
import com.root.model.Order;
import com.root.repository.CartRepository;
import com.root.repository.OrderRepository;
import com.root.service.CartService;
import com.root.service.OrderService;
import com.root.service.ProductService;
import com.root.service.UserService;

@RestController
@RequestMapping("/shoppingcart")
public class UserController {

	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	CartRepository cartRepo;
	
	@Autowired
	CartService cartService;		
	
	@Autowired
	UserService userService;

	@Autowired
	ProductService productService;
	
	@GetMapping("/hello")
	public String hello() {
		return "Hello, How you doing ?";
	}

	@GetMapping("/user")
	public String getUser() {	
		return "Hello User";
	}

	
	@PostMapping("/user/additemtocart")
	public ResponseEntity<Cart> addtocart(@RequestParam("productName") String productname, @RequestParam("quantity") int quan) throws ProductExceptions
	{
		Cart cart =  cartService.addToCart(productname, quan);
		return new ResponseEntity<Cart>(cart,HttpStatus.OK);
	}
		
	@GetMapping("/user/viewcart")
	public ResponseEntity<Cart> viewMyCart()
	{
		Cart cart = cartService.viewMyCart();
		return new ResponseEntity<Cart>(cart,HttpStatus.OK);
	}
	
	@PutMapping("/user/removeitemfromcart")
	public String removefromcart(@RequestParam("productName") String productname) throws ProductExceptions
	{
		return cartService.removeItemFromCart(productname);
	}
	
	
	@DeleteMapping("/admin/deletecart/{id}")
	public String deletecart(@PathVariable String id)
	{
		return cartService.deleteCart(id);
	}
	
	
	@GetMapping("/user/buyproducts")
	public ResponseEntity<Order> buyproducts()
	{
		Order order = orderService.buyProduct();
		return new ResponseEntity<Order>(order,HttpStatus.OK);		
	}
	
	@GetMapping("/user/vieworders")
	public ResponseEntity<Order> viewOrderDetails()
	{
		Order order = orderService.viewOrders();
		return new ResponseEntity<Order>(order,HttpStatus.OK);
	}
	
}
