package com.root.controller.advice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.root.exception.CartExceptions;
import com.root.exception.OrderExceptions;
import com.root.exception.ProductExceptions;
import com.root.exception.ProductQuantityException;
import com.root.service.CartService;

@ControllerAdvice
public class ControllersAdvice {

	@Autowired
	CartService cartService;
	
	
	@ExceptionHandler(CartExceptions.class)
	public ResponseEntity<String> handleEmplyCart(CartExceptions cartIsEmpty)
	{
		return new ResponseEntity<String>(cartIsEmpty.getMessage(),HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ProductExceptions.class)
	public ResponseEntity<String> handleProductNotFound(ProductExceptions productException)
	{
		return new ResponseEntity<String>(productException.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ProductQuantityException.class)
	public ResponseEntity<String> handleQuantityException(ProductQuantityException quanityException)
	{
		return new ResponseEntity<String>(quanityException.getMessage(),HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(OrderExceptions.class)
	public ResponseEntity<String> handleEmptyOrderList(OrderExceptions orderException)
	{
		return new ResponseEntity<String>(orderException.getMessage(),HttpStatus.BAD_REQUEST);
	}
}
