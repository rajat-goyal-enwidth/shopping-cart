package com.root.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.stereotype.Repository;

import com.root.model.Product;

@Repository
public interface ProductRepository extends ElasticsearchRepository<Product, String>{

}

