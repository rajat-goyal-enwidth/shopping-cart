package com.root.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.stereotype.Repository;

import com.root.model.DAOUser;

@Repository
public interface UserRepository extends ElasticsearchRepository<DAOUser, String> //JpaRepository<DAOUser, String>{
{
	DAOUser findByUsername(String username);
}
