package com.root.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.stereotype.Repository;

import com.root.model.Order;

@Repository
public interface OrderRepository extends ElasticsearchRepository<Order, String>{

}
