package com.root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.root.model.DAOUser;
import com.root.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;
	
	public void saveUserCart(String username, String userCartId)
	{
		DAOUser user = userRepo.findByUsername(username);
		System.out.println("usercartID :- "+userCartId);
		DAOUser saveUserCart = new DAOUser(user.getId(),user.getEmail(),user.getUsername(),
				user.getPassword(),user.getRole(),userCartId);			
		
		userRepo.save(saveUserCart);
	}
	
	
	public DAOUser currentUserDetails()
	{
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
		.getPrincipal();	
		
		String username = userDetails.getUsername();	
									
		DAOUser user = userRepo.findByUsername(username);
		
		return user;
	}
}
