package com.root.service;

import java.util.ArrayList; 
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.root.config.ElasticSearchConfig;
import com.root.exception.ProductExceptions;
import com.root.model.Product;
import com.root.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepo;
	
	
	@Autowired
	ElasticSearchConfig es;
	
	public String saveProduct(Product product)
	{
		productRepo.save(product);
		return "Product "+product.getProductName()+" , saved successfully";
	}
	
	public Product findproductbyid(String id) throws ProductExceptions
	{
		Optional<Product> findproduct = productRepo.findById(id);
		if(findproduct.isEmpty())
		{
			throw new ProductExceptions("Sorry, Product "+id+"  is not available");		
		}
		
		Product product = new Product(findproduct.get().getId(),findproduct.get().getProductName()
				,findproduct.get().getProductPrice(),findproduct.get().getProductDetails()
				,findproduct.get().getQuantity());
		return product;		
	}
	
	public List<Product> findallproducts()
	{
		List<Product> products = new ArrayList<>();
		productRepo.findAll().forEach(products::add);
		return products;
	}
	
	public Product updateProductPrice(String id, double price) throws ProductExceptions
	{
		Optional<Product> updateproduct = productRepo.findById(id);
		Product product = new Product();
		product.setId(updateproduct.get().getId());
		product.setProductName(updateproduct.get().getProductName());
		product.setProductPrice(updateproduct.get().getProductPrice());
		product.setProductDetails(updateproduct.get().getProductDetails());
		product.setQuantity(updateproduct.get().getQuantity());
		
		if(product != null )
		{
			product.setProductPrice(price);
			productRepo.save(product);
			return product;
		}
		
		throw new ProductExceptions("Product "+product.getProductName()+" not found");
		
	}
	
	public String deleteProduct(String id) throws ProductExceptions
	{
		Optional<Product> product = productRepo.findById(id);
		if(product != null)
		{
			String message = "Product "+product.get().getProductName()+" deleted successfully";
			productRepo.deleteById(product.get().getId());
			return message;
		}
		throw new ProductExceptions("Product "+product.get().getProductName()+" not found");
	}
	

	public Product findproductByname(String productName) throws ProductExceptions
	{
		List<Product> products = new ArrayList<>();
		productRepo.findAll().forEach(products::add);
		
		Product product = new Product();

		for(Product p : products)
		{		
			if(p.getProductName().equals(productName)) 
			{
				product.setId(p.getId());
				product.setProductName(p.getProductName());
				product.setProductPrice(p.getProductPrice());
				product.setProductDetails(p.getProductDetails());
				product.setQuantity(p.getQuantity());
				return product;
			}
		}
			
		throw new ProductExceptions("Product "+productName+" not found");
		
	}
	
	
	public String updateProductQuantity(String id, int quantity) throws ProductExceptions
	{
		Optional<Product> updateproduct = productRepo.findById(id);
		Product product = new Product();
		product.setId(updateproduct.get().getId());
		product.setProductName(updateproduct.get().getProductName());
		product.setProductPrice(updateproduct.get().getProductPrice());
		product.setProductDetails(updateproduct.get().getProductDetails());
		product.setQuantity(quantity);
		productRepo.save(product);
		
		return "Updated";
	}
	
}
