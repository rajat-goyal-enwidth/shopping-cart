package com.root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.root.model.DAOUser;
import com.root.repository.UserRepository;

@Service
public class AdminService {

	@Autowired
	UserRepository userRepo;

	public DAOUser findUser(String username) throws UsernameNotFoundException
	{	
		DAOUser user = userRepo.findByUsername(username);
		if(user != null)
		{
			return user;
		}
		
		throw new UsernameNotFoundException("User not found "+username);		
	}
	
	public String deleteUser(String username) throws UsernameNotFoundException
	{
		DAOUser user = userRepo.findByUsername(username);
		if( user != null)
		{
			String message = "User Id :- "+user.getId()+" User Name :- "+user.getUsername()+" deleted successfully";
			userRepo.deleteById(user.getId());
			return message;
		}
		
		throw new UsernameNotFoundException("User not found "+username);	
	}
}
