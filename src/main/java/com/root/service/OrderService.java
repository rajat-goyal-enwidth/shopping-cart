package com.root.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.exception.CartExceptions;
import com.root.exception.OrderExceptions;
import com.root.model.Cart;
import com.root.model.DAOUser;
import com.root.model.Order;
import com.root.model.Product;
import com.root.repository.CartRepository;
import com.root.repository.OrderRepository;
import com.root.repository.ProductRepository;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepo;

	@Autowired
	UserService userService;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	CartRepository cartRepo;
	
	@Autowired
	CartService cartService;
	
	public Order buyProduct() throws CartExceptions
	{
		Cart userCart = cartService.viewMyCart();
		List<Product> products = userCart.getProduct();
		if(products.isEmpty())
		{
			throw new CartExceptions("Sorry, Your cart is empty !\nPlease add some products to buy");
		}
		
		List<Product> orderProducts = new ArrayList<>();
		Cart cart = new Cart();
		double price = 0;
		int totalproducts = 0;
		
		
		double totalamount=0;		
		int quantity=0;
		
				
		for(Product product : products)
		{					
			//for adding all the products from the cart to order
			Product p = new Product();
			p.setProductName(product.getProductName());
			p.setProductDetails(product.getProductDetails());
			p.setProductPrice(product.getProductPrice());
			p.setQuantity(product.getQuantity());
			orderProducts.add(p);
						
			
			//calculating the quantity and total price for order
			price += product.getProductPrice() * product.getQuantity();
			totalproducts += product.getQuantity();		
			
						
			//updating product details after order the product
			Optional<Product> p1 = productRepo.findById(product.getId());
			Product p2 = new Product();
			p2.setId(p1.get().getId());
			p2.setProductName(p1.get().getProductName());
			p2.setProductDetails(p1.get().getProductDetails());
			p2.setProductPrice(p1.get().getProductPrice());
			p2.setQuantity(p1.get().getQuantity() - product.getQuantity());
			productRepo.save(p2);
			
			
		}
	
		Iterator<Product> iterator = products.iterator();
		while(iterator.hasNext())
		{		
			Product product = iterator.next();
			if(product!=null)
			{
				iterator.remove();
			}
		}
			
		
		//saving the order details 
		Order order = new Order();
		order.setUserId(userCart.getUserId());
		order.setProducts(orderProducts);
		order.setTotalamount(price);
		order.setTotalproducts(totalproducts);
		orderRepo.save(order);
		
		
		//updating cart details after order the products from the cart
		cart.setId(userCart.getId());
		cart.setUserId(userCart.getUserId());
		cart.setProduct(products);
		cart.setTotalamount(totalamount);
		cart.setTotalproducts(quantity);
		cartRepo.save(cart);
		
		return order;
	}
	
	
	public Order viewOrders() throws OrderExceptions
	{
		List<Order> orders = new ArrayList<>();
		orderRepo.findAll().forEach(orders::add);
		
		DAOUser user = userService.currentUserDetails();
		
		for(Order order : orders)
		{
			if(order.getUserId().equals(user.getId()))
			{
				if(order.getProducts().isEmpty())
				{
					throw new OrderExceptions("Your order list is empty since you did not order any product./nThankyou");
				}
				
				return order;
			}
		}
		
		throw new OrderExceptions("Sorry, your order list not found./nPlease check you cart first");
	}
}
