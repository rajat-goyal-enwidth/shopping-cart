package com.root.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.root.config.ElasticSearchConfig;
import com.root.exception.CartExceptions;
import com.root.exception.ProductExceptions;
import com.root.exception.ProductQuantityException;
import com.root.model.Cart;
import com.root.model.DAOUser;
import com.root.model.Product;
import com.root.repository.CartRepository;
import com.root.repository.UserRepository;

@Service
public class CartService {

	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	ElasticSearchConfig es;
	
	@Autowired
	CartRepository cartRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	UserService userService;
	
	
	public Cart viewMyCart() throws CartExceptions
	{
		Cart userCart = new Cart();
		//fetching current user details
		//retrieving userId from the database to save in particular cart which is associated with user
		DAOUser user = userService.currentUserDetails();
		
		String userCartId = user.getCartId();
		
		userCart.setId(userCartId);
		userCart.setUserId(user.getId());
		
		List<Product> pro = new ArrayList<>();
			
		if(userCartId.equals(""))
		{
			throw new CartExceptions("Sorry, Your cart is empty !");
		}
			Optional<Cart> cart = cartRepo.findById(userCartId); 
			
			List<Product> products = cart.get().getProduct();
			if(products.isEmpty())
			{
				throw new CartExceptions("Sorry, Your cart is empty !");
			}
			
		//	StringBuilder sb = new StringBuilder();
		//	sb.append("\n----------------------------\tYour Cart Details\t----------------------------\n");
			for(Product product : products)
			{
				Product p = new Product();
				p.setId(product.getId());
				p.setProductName(product.getProductName());
				p.setProductPrice(product.getProductPrice());
				p.setProductDetails(product.getProductDetails());
				p.setQuantity(product.getQuantity());
				
				pro.add(p);							
				
			//	sb.append("\nProduct Name : "+product.getProductName()+"\nProduct Price : "+product.getProductPrice()
			//	+"\nProduct Details : "+product.getProductDetails()+"\nQuantity : "+product.getQuantity()+"\n");
			}
			
			userCart.setProduct(pro);
			userCart.setTotalamount(cart.get().getTotalamount());
			userCart.setTotalproducts(cart.get().getTotalproducts());
			
//			System.out.println("cart details :- "+cartTest);
//			
//			sb.append("\nTotal Products : "+cart.get().getTotalproducts()+"\nTotal Amount : "+cart.get().getTotalamount());
//			
			//return sb.toString();
			return userCart;
	}
	
		
	public Cart addToCart(String productName, int quantity) throws ProductExceptions, ProductQuantityException
	{
		
		DAOUser user = userService.currentUserDetails();
		String userId = user.getId();
		
		String userCartId = user.getCartId();
		
		//retrieving product details to save in the cart and calculate the total amount	
		Product product = productService.findproductByname(productName);
		
		if(product.getQuantity() < quantity)
		{
			String str = "Sorry for the inconvenience,\nYou can buy only "+product.getQuantity()+"  "+product.getProductName();
			//throw new ProductQuantityException("" items\nThank you","400");
			throw new ProductQuantityException(str);
		}
				
		if(userCartId.equals(""))
		{		
			//creating products list to add one or more products in the cart
			List<Product> products = new ArrayList<>();
			product.setProductName(productName);
			product.setProductDetails(product.getProductDetails());
			product.setQuantity(quantity);
			products.add(product);
			
			
			//calculating total amount of the product based on quantity							
			Double productprice = product.getProductPrice();		
			double totalamount = productprice * quantity;
			
			
			//setting all the details in the cart
			Cart cart = new Cart();
			cart.setUserId(userId);
			cart.setProduct(products);
			cart.setTotalproducts(cart.getTotalproducts() + quantity);
			cart.setTotalamount(cart.getTotalamount()+totalamount);
			Cart cart1 = cartRepo.save(cart);
			userService.saveUserCart(user.getUsername(), cart1.getId());
			
//			String message = "Product "+productName+" has been added successfully in your cart \n "
//					+ "Total Amount :- "+cart.getTotalamount()+".";
//			
			return cart;
		}
		
		else
		{
			Optional<Cart> cart = cartRepo.findById(userCartId);
			Cart cartnew = new Cart();
			cartnew.setId(cart.get().getId());
			cartnew.setUserId(cart.get().getUserId());
			
			List<Product> products = cart.get().getProduct();
			Product product1 = productService.findproductByname(productName);
			product1.setProductName(productName);
			product1.setProductDetails(product.getProductDetails());
			product1.setQuantity(quantity);
			products.add(product1);
			
			Double productprice = product.getProductPrice();		
			double totalamount = productprice * quantity;
			
			cartnew.setProduct(products);
			cartnew.setTotalproducts(cart.get().getTotalproducts() + quantity);
			cartnew.setTotalamount(cart.get().getTotalamount()+totalamount);
			
			cartRepo.save(cartnew);
//			String message = "Product "+productName+" has been added successfully in your cart \n "
//					+ "Total Amount :- "+cartnew.getTotalamount()+".";
//			
			return cartnew;
		}
	}
	
	
	public String removeItemFromCart(String productName) throws ProductExceptions
	{
		DAOUser user = userService.currentUserDetails();
		String userId = user.getId();
				
		String userCartId = user.getCartId();
					
		Optional<Cart> cart = cartRepo.findById(userCartId);
		Cart cartnew = new Cart();
		cartnew.setId(cart.get().getId());
		cartnew.setUserId(cart.get().getUserId());
		
		Product product = productService.findproductByname(productName);
		
		double productPrice = product.getProductPrice();
		
		List<Product> products = cart.get().getProduct();	
		int productquantity = 0;
		int productindex = 0;
		for(Product p : products)
		{
			if(p.getProductName().equals(product.getProductName()))
			{
				productquantity = p.getQuantity();
				productindex = products.indexOf(p);
			}
		}
		
		products.remove(productindex);
		
		double totalamount = productPrice * productquantity;
		
		cartnew.setProduct(products);
		cartnew.setTotalproducts(cart.get().getTotalproducts() - productquantity);
		cartnew.setTotalamount(cart.get().getTotalamount()-totalamount);
		
		cartRepo.save(cartnew);
		
		String message = "Product "+productName+" has been removed from your cart \n "
				+ "Total Amount :- "+cartnew.getTotalamount()+".";
		
		return message;
	}
	
	
	public String deleteCart(String id)
	{
		cartRepo.deleteById(id);
		return "Cart deleted";
	}
	
	
}
