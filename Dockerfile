FROM openjdk:10
COPY ./target/shoppingcart.jar shoppingcart.jar
CMD ["java","-jar","shoppingcart.jar"] 